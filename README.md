
#  Image Classification with Keras

Free, Open source software can be used to classify photos.
It has a built-in ability to use an external model supplied by the user

## New changes

We have uploaded few more changes to this repo -

Project is Python3 compatible now.
Added TensorFlow support,
Added lots of performance improving changes. 

## How tu use 

#### Train

Keras models can be saved as a single [.hdf5 or h5] file, which stores both the architecture and weights, using the model.save() function. 
```
python Image_Class.py
    --c
    --train_path=""
    --test_path=""
    --class_1=""
    --class_2=""
    --model_path=""
```

#### Test

This model can be then load and be used to predict photos class.
```
python Image_Class.py
    --e
    --test_path=""
    --model_path=myModel
```

#### Optional args
```
    --epochs=
    --img_size=
```

#### Try
```
python xRay_eval.py --help
```
to learn about other supported flags.


# Example

Train neural network with images od cats and dogs.
```
python Image_Class.py
    -c
    --test_path=test 
    --train_path=train 
    --class_1=PNEUMONIA 
    --class_2=NORMAL 
    --model_path=myModel
    --epochs=2
    --img_size=100
```

Try to evaluate in with class test_path photo belongs to.
```
python Image_Class.py
    --e
    --test_path="cat3.jpg"
    --model_path="model"
```

## Dependencies

- keras
- tensorflow
- cv2
- tkinter
- numpy
- plaidml

## License

MIT