import argparse
import plaidml.keras
from keras_preprocessing import image
from keras_preprocessing.image import ImageDataGenerator

plaidml.keras.install_backend()
## add AMD support

import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
import tensorflow as tf
import numpy as np
import os
import cv2
from tkinter import *
from tkinter import filedialog
import random


#example of use with command line

# C:\Users\Dominik\PycharmProjects\pythonProject\venv\Scripts\python.exe ImagePROC.py --test_path=C:\Users\Dominik\PycharmProjects\pythonProject\test --train_path=C:\Users\Dominik\PycharmProjects\pythonProject\train --class_1=PNEUMONIA --class_2=NORMAL --model_path=model.txt -c

DEBUG_MODE = True
if DEBUG_MODE:
    print("debug information is enabled")

train_path_string = '--train_path'
test_path_string = '--test_path'
class1_string = '--class_1'
class2_string = '--class_2'
model_path_string = '--model_path'

train_path = ""
test_path = ""
class1 = ""
class2 = ""
model_path = ""
createModelFlag = False
testFlag = False


def bad_cmd_args(name, model = False):
    print("no proper args")
    print("error in " + name + " argument")
    if model:
        print("use one of the following flags: -c or -e")
    sys.exit()

ap = argparse.ArgumentParser()
ap.add_argument( "-d", nargs='?', required=False, help="use this flag to enable debug info to console", dest="debug", type=bool, default=False)

ap.add_argument( "-c", nargs='?', required=False, help="use this flag to create model", dest="createModelFlag", type=bool, default=False)
ap.add_argument( "-t", nargs='?', required=False, help="use this flag to test given photo", dest="testFlag", type=bool, default=False)
ap.add_argument( train_path_string, required=False, metavar='path', dest="train", help="path to folder with train data")
ap.add_argument( test_path_string, required=True, metavar='path', dest="test",help="path to folder with test data")
ap.add_argument( class1_string, required=True, metavar='path', dest="class1",help="first class name")
ap.add_argument( class2_string, required=True, metavar='path', dest="class2",help="second class name")
ap.add_argument( model_path_string, required=False, metavar='path', dest="model",default="model.hd5m", help="model name with path")


ap.add_argument( "--epochs", required=False, metavar='path', dest="epochs", help="no epochs", default=1, type=int)
ap.add_argument( "--img_size", required=False, metavar='path', dest="img_size", default=150, type=int)
ap.add_argument( "--batch_size", required=False, metavar='path', dest="batch_size", default=32, type=int)

args2 = vars(ap.parse_args())
args = ap.parse_args()

if args.debug != False:
    DEBUG_MODE = True
else:
    DEBUG_MODE = False

if DEBUG_MODE:
    print
    print(args)
    print(args2)
    print
    print(args.testFlag)
    print(args.createModelFlag)
    print(args.train)
    print(args.test)
    print(args.class1)
    print(args.class2)
    print(args.model)
    print
    print(args.epochs)
    print(args.img_size)
    print(args.batch_size)
    print

    
if args.createModelFlag != False:
    createModelFlag = True
if args.testFlag != False:
    testFlag = True


if ((not createModelFlag) and (not testFlag)):
    bad_cmd_args("flag", True)


if not args.test:
    bad_cmd_args(test_path_string)
if not args.class1:
    bad_cmd_args(class1_string)
if not args.class2:
    bad_cmd_args(class2_string)
if not args.model:
    bad_cmd_args(model_path_string)

if (not args.train) and (createModelFlag):
    bad_cmd_args(train_path_string)

if DEBUG_MODE:
    print
    print("proper args")

# region of passing cmd arguments

CLASS_NAMES = [args.class1, args.class2]
data_dir = args.train
validateDir = args.test

model_name = args.model

img_size = args.img_size
epochs = args.epochs
batchSize = args.batch_size

# start of the exact program

#img_size = 100
#epochs = 1
#batchSize = 32

#CLASS_NAMES = ['PNEUMONIA', 'NORMAL']
#data_dir = r"C:\Users\Dominik\PycharmProjects\pythonProject\train"
#validateDir = r"C:\Users\Dominik\PycharmProjects\pythonProject\test"

good_guess:int = 0
bad_guess:int = 0

training_data = []

toRecognize = [0, 0]
recognizedCount = [0, 0]
trainData = [0, 0]
train_generator = 0


filePath = ""
modelPath = ""
root = 0



def create_training_data():
    global toRecognize
    for class_name in CLASS_NAMES:
        path = os.path.join(data_dir, class_name)
        class_num = CLASS_NAMES.index(class_name)
        for img in os.listdir(path):
            try:
                img_array = cv2.imread(os.path.join(path, img), cv2.IMREAD_GRAYSCALE)
                new_array = cv2.resize(img_array, (img_size, img_size))
                training_data.append([new_array, class_num])
                global trainData
                trainData[class_num] += 1
            except:
                print(sys.exc_info()[0])
                pass
    return training_data


def train():
    X = []
    y = []

    for features, label in training_data:
        X.append(features)
        y.append(label)

    X = np.reshape(
        a=X,
        newshape=(-1, img_size, img_size, 1)
    )

    X = X / 255.0
    y = np.array(y)

    train_datagen = ImageDataGenerator(rotation_range=10,
                                       shear_range=0.1,
                                       zoom_range=0.05,
                                       fill_mode='nearest')

    global train_generator
    train_generator = train_datagen.flow(X,
                                         y,
                                         batch_size=batchSize)

    model = Sequential()
    model.add(Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=X.shape[1:]))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Conv2D(64, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.4))

    model.add(Conv2D(128, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Conv2D(256, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.4))
    model.add(Conv2D(512, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))

    model.compile(
        loss='binary_crossentropy',
        # optimizer='adam',
        optimizer=keras.optimizers.Adadelta(),
        metrics=['accuracy'])

    model.fit_generator(train_generator,
                        steps_per_epoch=len(X) // batchSize,
                        epochs=epochs,
                        shuffle=TRUE
                        )

    # model.fit(
    #     X,
    #     y,
    #     batch_size=batchSize,
    #     epochs=epochs)

    model.save('RTG')
    return model


def result(model, imgToPredict, name, class_name):
    resultt = model.predict_classes(imgToPredict)

    global recognizedCount

    if resultt[0][0] == 1:
        recognizedCount[1] += 1
        recognized = CLASS_NAMES[1]
    else:
        recognizedCount[0] += 1
        recognized = CLASS_NAMES[0]

    print(recognized)
    print(name)
    if class_name == recognized:
        global good_guess
        good_guess += 1
    else:
        global bad_guess
        bad_guess += 1

    return recognized


def loadIMG(path):
    img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    img = cv2.resize(img, (img_size, img_size))
    img = image.img_to_array(img)
    img = img.reshape(-1, img_size, img_size, 1)
    return img


def checkAll(model):
    for class_name in CLASS_NAMES:
        path = os.path.join(validateDir, class_name)
        class_num = CLASS_NAMES.index(class_name)
        for img in os.listdir(path):
            try:
                img_to_pred = loadIMG(os.path.join(path, img))
                result(model, img_to_pred, os.path.join(path, img), class_name)
                toRecognize[class_num] += 1
            except:
                print(sys.exc_info()[0])
                pass

def chceckOne(model, path):
    try:
        img_to_pred = loadIMG(path)
        resultt = model.predict_classes(img_to_pred)
        print("algorithm classify imege to class:")
        if resultt[0][0] == 1:
            print(class2_string)
        else:
            print(class1_string)
        print()
    except:
        print(sys.exc_info()[0])
        pass



def show_predict():
    global filePath
    global modelPath

    print(modelPath)

    #model2 = tf.models.load_model('RTG.model')
    #model2 = tf.keras.models.load_model(modelPath)
    model2 = tf.keras.models.load_model('RTG')

    imageToChcek = loadIMG(filePath)
    result2 = result(model2, imageToChcek, "", "")
    Label(root, text=result2).pack()


def loadFile():
    global filePath
    filePath = filedialog.askopenfilename()


def loadModel():
    global modelPath
    modelPath = filedialog.askopenfilename(filetypes=(("model files", ".model"), ("all files", ".*")))
    Label(root, text="Wczytano model").pack()


def initGUI():
    global root
    root = Tk()
    root.title("X-RAY")
    root.geometry("300x300")

    btnModel = Button(root, text="Load model", command=loadModel).pack()
    btnPhoto = Button(root, text="Load file", command=loadFile).pack()
    btnCheck = Button(root, text="Check photo!", command=show_predict).pack()
    root.mainloop()


def printStats():
    print("\n")
    print("dane uczace "+str(CLASS_NAMES[0]).upper()+": " + str(trainData[0]))
    print("do rozpoznania "+str(CLASS_NAMES[0]).upper()+": " + str(toRecognize[0]))
    print("wybor opcji "+str(CLASS_NAMES[0]).upper()+": " + str(recognizedCount[0]))
    print("\n")
    print("dane uczace "+str(CLASS_NAMES[1]).upper()+": " + str(trainData[1]))
    print("do rozpoznania "+str(CLASS_NAMES[1]).upper()+": " + str(toRecognize[1]))
    print("wybor opcji "+str(CLASS_NAMES[1]).upper()+": " + str(recognizedCount[1]))
    print("\n")
    print("suma testow: " + str(good_guess + bad_guess))
    print("dobrze rozpoznane: " + str(good_guess))
    print("zle rozpoznanie: " + str(bad_guess))
    print('\n')
    print("accuracy:" + str(good_guess / (good_guess + bad_guess)))

print()
print("starting")
print()


if (createModelFlag):
    create_training_data()
    random.shuffle(training_data)
    model = train()
    checkAll(model)
    printStats()
else:
    create_training_data()
    random.shuffle(training_data)
    model = train()
    #model = tf.keras.models.load_model('RTG')
    chceckOne(model, test_path)
